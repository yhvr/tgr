const reload = new Layer({
	resetArgs() {
		return ["x", "y", "z", "upgrades"];
	},
	gain() {
		let out = game.z;

		if (game.upgrades.rel1) out = out.mul(2);
		out = softcap(out, new Decimal(100), .75)
		out = softcap(out, new Decimal(1e5))

		return out;
	},
	mult(points) {
		return points.div(2).add(1)
	},
	minGain: new Decimal(1),
	currName: "reloadPoints",
	floorGain: true,
});
