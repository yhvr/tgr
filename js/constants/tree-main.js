constants.trees.main = {
	0: {
		short: "ɑ",
		desc: "Start producing x.",
		curr: "x",
		cost: new Decimal(0),
		top: 0,
		left: 0,
		children: [1],
		parent: 0,
	},
	1: {
		short: "β",
		desc: "Increase x production by 1/s.",
		curr: "x",
		cost: new Decimal(5),
		top: 1,
		left: 0,
		children: [2],
	},
	2: {
		short: "γ",
		desc: "Multiply x production by 2.",
		curr: "x",
		cost: new Decimal(15),
		top: 1,
		left: 1,
		children: [3, 4],
	},
	3: {
		short: "δ",
		desc: "Multiply x production by 5.",
		curr: "x",
		cost: new Decimal(50),
		top: 0,
		left: 1,
		children: [],
	},
	4: {
		short: "ε",
		desc: "Increase x/s by 100.",
		curr: "x",
		cost: new Decimal(200),
		top: 2,
		left: 1,
		children: [5, 7],
	},
	5: {
		short: "ζ",
		desc: "Raise x production to the power of 1.5.",
		curr: "x",
		cost: new Decimal(1000),
		top: 2,
		left: 0,
		children: [6, 20],
	},
	6: {
		short: "η",
		desc: "Multiply x production by 2.",
		curr: "x",
		cost: new Decimal(15000),
		top: 2,
		left: -1,
		children: [17],
	},
	7: {
		short: "θ",
		desc: "Increase ζ power from 1.5 -> 2.",
		curr: "x",
		cost: new Decimal(35000),
		top: 2,
		left: 2,
		children: [8, 9, 10],
	},
	8: {
		short: "ι",
		desc: "Increase x/s by 1e5.",
		curr: "x",
		cost: new Decimal(500000),
		top: 1,
		left: 2,
		children: [11, 13],
	},
	9: {
		short: "κ",
		desc: "Multiply x gain by log10(x).",
		curr: "x",
		cost: new Decimal(2.5e6),
		top: 3,
		left: 2,
		children: [16],
	},
	10: {
		short: "λ",
		desc: "Start producing y.",
		curr: "x",
		cost: new Decimal(2.5e7),
		top: 2,
		left: 3,
		children: [12],
	},
	11: {
		short: "μ",
		desc: "Multiply x gain based on y. (y^(1/6))",
		curr: "y",
		cost: new Decimal(2.5),
		top: 0,
		left: 2,
		children: [14],
	},
	12: {
		short: "ν",
		desc: "Increase y gain based on x. (log10(x))",
		curr: "x",
		cost: new Decimal(1e8),
		top: 3,
		left: 3,
		children: [15],
	},
	13: {
		short: "ξ",
		desc: "Multiply x gain by 10.",
		curr: "x",
		cost: new Decimal(2.5e8),
		top: 1,
		left: 3,
		children: [21],
	},
	14: {
		short: "ο",
		desc: "Multiply y gain by 3.",
		curr: "y",
		cost: new Decimal(100),
		top: 0,
		left: 3,
		children: [22],
	},
	15: {
		short: "⟳",
		cDesc() {
			return `Reload.<br>
You'd gain <strong>${format(reload.gain())}</strong> reload points<br>
if you reloaded now.<br>
<br>
Each reload point you have (${format(game.reloadPoints)})<br>
gives you a 50% production bonus (${format(reload.getMult())}x).`;
		},
		curr: "y",
		cost: new Decimal(Infinity),
		top: 2,
		left: 4,
		children: [],
		sizeMult: 2.5,
		buyAction() {
			reload.reset();
		},
		glow() {
			return reload.gain().gt(game.reloadPoints.div(5));
		},
		shown() {
			return game.reloadPoints.gte(1) || game.upgrades[12];
		},
	},
	16: {
		short: "π",
		desc: "Multiply y gain by 3.14.",
		curr: "y",
		cost: new Decimal(314),
		top: 3,
		left: 1,
		children: [],
	},
	17: {
		short: "ρ",
		desc: "Multiply x production by 5.",
		curr: "y",
		cost: new Decimal(500),
		top: 1,
		left: -1,
		children: [18, 19],
	},
	18: {
		short: "σ",
		desc: "Multiply y gain by 4.",
		curr: "y",
		cost: new Decimal(7500),
		top: 0,
		left: -1,
		children: [],
	},
	19: {
		short: "τ",
		desc: "Increase y gain by 100/s.",
		curr: "x",
		cost: new Decimal(1e9),
		top: 1,
		left: -2,
		children: [],
	},
	20: {
		short: "υ",
		desc: "Start producing z.",
		curr: "x",
		cost: new Decimal(2.5e9),
		top: 3,
		left: 0,
		children: [],
	},
	21: {
		short: "φ",
		desc: "Multiply z gain by 3.",
		curr: "z",
		cost: new Decimal(0.3),
		top: 1,
		left: 4,
		children: [],
	},
	22: {
		short: "χ",
		desc: "Multiply x gain by 7.",
		curr: "y",
		cost: new Decimal(1e4),
		top: 0,
		left: 4,
		children: [23],
	},
	23: {
		short: "ψ",
		desc: "Multiply z gain based on y (log10(y)).",
		curr: "y",
		cost: new Decimal(1e6),
		top: 0,
		left: 5,
		children: [24],
	},
	24: {
		short: "ω",
		desc:
			"Wait, I'm out of Greek letters? Shoot.<br>Uh, I mean, multiply z gain by 10.",
		curr: "x",
		cost: new Decimal(1e15),
		top: 1,
		left: 5,
		children: [],
	},
	rel1: {
		short: "a",
		desc: "Double reload point gain.",
		curr: "reloadPoints",
		cost: new Decimal(5),
		top: -1,
		left: 0,
		children: ["rel2", "rel3"],
		shown() {
			return game.reloadPoints > 0;
		},
		parent: "rel1",
	},
	rel2: {
		short: "b",
		desc: "Multiply y gain by 10.",
		curr: "reloadPoints",
		cost: new Decimal(1e3),
		top: -1,
		left: 1,
		children: [],
	},
	rel3: {
		short: "c",
		desc: "Multiply x gain based on z (z).",
		curr: "reloadPoints",
		cost: new Decimal(1e6),
		top: -2,
		left: 0,
		children: [],
	},
};

constants.trees.main = mapObj(constants.trees.main, (c, id) => {
	c.top += constants.size.upgs_before_start_top;
	c.left += constants.size.upgs_before_start_left;
	c.sizeMult = c.sizeMult || 1;
	c.width = c.height = 50 * c.sizeMult;
	c.children.forEach(child => {
		constants.trees.main[child].parent = id;
	});
	return c;
});
