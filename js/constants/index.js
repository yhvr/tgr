const constants = {
	trees: {},
	size: {
		upgs_before_start_left: 2,
		upgs_before_start_top: 2,
		upg_size: 75,
		upg_w: 8,
		upg_h: 5,
	},
};

function mapObj(obj, map) {
	for (const key in obj) {
		obj[key] = map(obj[key], key, obj)
	}
	return obj;
}