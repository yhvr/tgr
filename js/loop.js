function nextFrame() {
	const time = Date.now();
	let dt = (time - game.lastFrame) / 1000;
	if (dt > 1 && game.dev) dt = 1;
	game.lastFrame = time;

	if (app.tooltipShown)
		app.tooltipShown = app.tooltipNode.cDesc
			? app.tooltipNode.cDesc()
			: `${app.tooltipNode.desc}<br>
Cost: ${format(app.tooltipNode.cost)} ${app.fancy[app.tooltipNode.curr]}`;

	// upgrade order is a lot simpler this time around.
	// "the way you're supposed to buy them in".
	if (game.upgrades[0]) {
		let x = new Decimal(1);

		if (game.upgrades[1]) x = x.add(1);
		if (game.upgrades[2]) x = x.mul(2);
		if (game.upgrades[3]) x = x.mul(5);
		if (game.upgrades[4]) x = x.add(100);
		if (game.upgrades[5]) x = x.pow(game.upgrades[7] ? 2 : 1.5);
		if (game.upgrades[6]) x = x.mul(2);
		if (game.upgrades[8]) x = x.add(1e5);
		if (game.upgrades[9]) x = x.mul(game.x.add(1).log10().add(1));
		if (game.upgrades[11]) x = x.mul(game.y.pow(1 / 6).max(1));
		if (game.upgrades[13]) x = x.mul(10);
		if (game.upgrades[22]) x = x.mul(7);
		if (game.upgrades.rel3) x = x.mul(game.z.max(1));

		x = x.mul(reload.getMult());

		app.perSec.x = x;

		game.x = game.x.add(x.mul(dt));
	}

	if (game.upgrades[10]) {
		let y = new Decimal(0.1);

		if (game.upgrades[12]) y = y.add(game.x.log10().max(0));
		if (game.upgrades[14]) y = y.mul(3);
		if (game.upgrades[16]) y = y.mul(3.14);
		if (game.upgrades[19]) y = y.add(10);
		if (game.upgrades[18]) y = y.mul(4);
		if (game.upgrades.rel2) y = y.mul(10);

		y = y.mul(reload.getMult());

		app.perSec.y = y;

		game.y = game.y.add(y.mul(dt));
	}

	if (game.upgrades[20]) {
		let z = new Decimal(0.01);

		if (game.upgrades[21]) z = z.mul(3);
		if (game.upgrades[23]) z = z.mul(game.y.log10().max(0));
		if (game.upgrades[24]) z = z.mul(10);

		z = z.mul(reload.getMult());

		app.perSec.z = z;

		game.z = game.z.add(z.mul(dt));
	}

	window.requestAnimationFrame(nextFrame);
}

window.requestAnimationFrame(nextFrame);
