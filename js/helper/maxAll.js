const noMax = [15]

function maxAll() {
	if (game.reloadPoints.lt(1e6)) return;
	if (!game.upgrades[0]) buyUpg(0);
	for (id in game.upgrades) {
		if (!game.upgrades[id]) continue;
		const node = constants.trees.main[id];
		node.children.forEach(child => {
			if (game.upgrades[child] || noMax.includes(child)) return;
			buyUpg(child);
		})
	}
}