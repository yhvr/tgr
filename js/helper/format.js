function format(num) {
	if (num.lt(10)) return num.toFixed(2);
	if (num.lt(100)) return num.toFixed(1);
	if (num.lt(1e3)) return num.toFixed(0);
	if (num.lt(1e9)) return num.floor().toNumber().toLocaleString();
	if (num.eq(new Decimal(Infinity))) return "∞"
	let exponent = num.log10().floor();
	let mantissa = num.div(new Decimal(10).pow(exponent));
	return `${mantissa.toFixed(2)}e${exponent}`;
}
