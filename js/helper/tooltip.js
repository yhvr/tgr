let currentCoords = [0, 0];

function showTooltip(id) {
	app.tooltipNode = constants.trees.main[id];
	app.tooltipShown = app.tooltipNode.cDesc
		? app.tooltipNode.cDesc()
		: `${app.tooltipNode.desc}<br>
Cost: ${format(app.tooltipNode.cost)} ${app.fancy[app.tooltipNode.curr]}`;
	app.tooltipCoords = currentCoords;
}

function hideTooltip(id) {
	app.tooltipShown = false;
}

window.addEventListener("mousemove", e => {
	currentCoords = [e.clientX, e.clientY];
});
