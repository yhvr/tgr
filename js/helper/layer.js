class Layer {
	constructor({ resetArgs, gain, currName, minGain, floorGain, mult }) {
		this.resetArgs = resetArgs;
		this._gain = gain;
		this.currName = currName;
		this.minGain = minGain;
		this.floorGain = floorGain;
		this.mult = mult;
	}

	gain() {
		let gain = this._gain();
		if (this.floorGain) gain = gain.floor();
		return gain;
	}

	reset() {
		const gain = this.gain();
		if (!this.canReset(gain)) return;
		game[this.currName] = game[this.currName].add(gain);
		const args = this.resetArgs(gain);
		args.forEach(arg => {
			game[arg] = defaultGame[arg];
			app.game[arg] = defaultGame[arg];
			if (arg === "upgrades") {
				$("canvas").getContext("2d").clearRect(0, 0, 10000, 10000);
				game.upgrades = {};
			}
		});
	}

	// put gain as an arg instead of calculated in
	// real-time to marginally increase performance
	canReset(n = this.gain()) {
		return n.gte(this.minGain);
	}

	getMult() {
		return this.mult(game[this.currName]);
	}
}
