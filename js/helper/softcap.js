// Originally made by Acamaeda for The Modding Tree.

function softcap(value, cap, power = 0.5) {
	if (value.lte(cap)) return value;
	return value.pow(power).times(cap.pow(new Decimal(1).sub(power)));
}
