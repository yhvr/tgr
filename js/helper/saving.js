function save() {
	let out = {};
	for (const key in game) {
		if (game[key] instanceof Decimal) out[key] = game[key].toString();
		else out[key] = game[key];
	}
	localStorage.setItem("yhvr_tgr_save", JSON.stringify(out));
}

window.setInterval(() => ((game.autoSave && shouldSave) ? save : () => {})(), 5000)
let shouldSave = true;

function reset() {
	game = defaultGame;
	app.game = defaultGame;
	$("canvas").getContext("2d").clearRect(0, 0, 10000, 10000)
	app.$forceUpdate()
}

// TODO make automatic
const decimals = ["x", "y", "z", "reloadPoints"];

function load() {
	if (!localStorage.getItem("yhvr_tgr_save")) return;
	const sv = JSON.parse(localStorage.getItem("yhvr_tgr_save"));
	for (const key in sv) {
		if (decimals.includes(key)) game[key] = new Decimal(sv[key]);
		else game[key] = sv[key];
	}

	$("canvas").getContext("2d").clearRect(0, 0, 10000, 10000);
	for (const upg in game.upgrades) {
		if (!game.upgrades[upg]) return;
		constants.trees.main[upg].children.forEach(child => drawLine(upg, child, "main"));
	}
}
