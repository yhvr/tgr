function buyUpg(id, tree = "main") {
	const node = constants.trees[tree][id];
	if (node.buyAction) {
		node.buyAction();
		return;
	}
	if (game[node.curr].lt(node.cost)) return;
	if (game.upgrades[id]) return;
	game[node.curr] = game[node.curr].sub(node.cost);
	game.upgrades[id] = true;;
	node.children.forEach(child => {
		drawLine(id, child, tree);
	});
}
