const $ = _ => document.querySelector(_);

const app = new Vue({
	el: "#main",
	data: {
		game,
		c: constants,
		buyUpg,
		showTooltip,
		hideTooltip,
		format,
		tooltipShown: false,
		tooltipCoords: [0, 0],
		tooltipNode: {},
		fancy: {
			x: "x",
			y: "y",
			z: "z",
			reloadPoints: "reload points"
		},
		perSec: {
			x: new Decimal(0),
			y: new Decimal(0),
			z: new Decimal(0),
		}
	},
	computed: {
		tree() {
			return constants.trees.main;
		},
	},
});

const panzoom = new Panzoom($("#panzoom"), {
	canvas: true,
	maxScale: 1,
	minScale: .25,
	excludeClass: "no-pz",
	get exclude() {
		return Array.from(document.querySelectorAll(".tree-btn"));
	},
});

$("#panzoom").parentElement.addEventListener("wheel", panzoom.zoomWithWheel);
setTimeout(() =>
	panzoom.pan(
		window.innerWidth / 2 -
			75 * (constants.size.upgs_before_start_left + 1),
		window.innerHeight / 2 -
			75 * (constants.size.upgs_before_start_top + 1)
	)
);

function drawLine(from, to, tree) {
	let ctx = $("canvas").getContext("2d");
	let n1 = constants.trees[tree][from];
	let x1 = n1.left * constants.size.upg_size + (n1.width / 2) - 3;
	let y1 = n1.top * constants.size.upg_size + (n1.height / 2) - 3;
	let n2 = constants.trees[tree][to];
	let x2 = n2.left * constants.size.upg_size + (n2.width / 2) - 3;
	let y2 = n2.top * constants.size.upg_size + (n2.height / 2) - 3;
	ctx.strokeStyle = "#ffffff";
	ctx.lineWidth = 6;
	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.stroke();
}
