let game = {
	x: new Decimal(0),
	y: new Decimal(0),
	z: new Decimal(0),
	reloadPoints: new Decimal(0),
	upgrades: {},
	lastFrame: Date.now(),
	dev: false,
	autoSave: true,
};

let defaultGame = _.cloneDeep(game);